import _ from "lodash";
import { call, put, takeLatest } from "redux-saga/effects";
import TypeActions from "../TypeActions/index";
import ServiceURL from "../../Service/ServiceURL";
import { DELETE, GET, PATCH, POST } from "../../Service/ServiceBase";

export function* getListEvent(data) {
  const url = ServiceURL.Event + "?" + data.params;
  const callback = data.callback;
  try {
    const res = yield call(GET, url);

    if (res.message && !_.isEmpty(res.message)) {
      yield put({
        type: TypeActions.GET_LIST_EVENT_FAILED,
        error: res.message,
      });
    } else {
      yield put({
        type: TypeActions.GET_LIST_EVENT_SUCCESS,
        data: res.data.results,
      });
      callback && callback.success(res.data);
    }
  } catch (error) {
    yield put({ type: TypeActions.GET_LIST_EVENT_FAILED, error });
    callback && callback.failed(error.response.data.message);
  }
}
export function* getEvent(data) {
  const url = ServiceURL.Event + "/" + data.params;
  const callback = data.callback;
  try {
    const res = yield call(GET, url);

    if (res.message && !_.isEmpty(res.message)) {
      yield put({
        type: TypeActions.GET_EVENT_FAILED,
        error: res.message,
      });
      callback && callback.failed(res.message);
    } else {
      yield put({
        type: TypeActions.GET_EVENT_SUCCESS,
        data: res.data.results,
      });
      callback && callback.success(res.data);
    }
  } catch (error) {
    yield put({ type: TypeActions.GET_EVENT_FAILED, error });
    callback && callback.failed(error.response.data.message);
  }
}

export function* createEvent(data) {
  const url = ServiceURL.Event;
  const callback = data.callback;
  try {
    const res = yield call(POST, url, data.body);
    if (res.message && !_.isEmpty(res.message)) {
      yield put({
        type: TypeActions.CREATE_EVENT_FAILED,
        error: res.message,
      });
    } else {
      yield put({
        type: TypeActions.CREATE_EVENT_SUCCESS,
        data: res.data.results,
      });
      callback && callback.success();
    }
  } catch (error) {
    yield put({ type: TypeActions.CREATE_EVENT_FAILED, error });
    callback && callback.failed(error.response.data.message);
  }
}

export function* deleteEvent(data) {
  const url = ServiceURL.Event + "/" + data.params;
  const callback = data.callback;
  try {
    const res = yield call(DELETE, url);
    if (res.message && !_.isEmpty(res.message)) {
      yield put({
        type: TypeActions.DELETE_EVENT_FAILED,
        error: res.message,
      });
    } else {
      yield put({
        type: TypeActions.DELETE_EVENT_SUCCESS,
        data: res.data.results,
      });
      callback && callback.success();
    }
  } catch (error) {
    yield put({ type: TypeActions.DELETE_EVENT_FAILED, error });
    callback && callback.failed(error.response.data.message);
  }
}

export function* updateEvent(data) {
  const url = ServiceURL.Event + "/" + data.params;
  const callback = data.callback;
  try {
    const res = yield call(PATCH, url, data.body);
    if (res.message && !_.isEmpty(res.message)) {
      yield put({
        type: TypeActions.UPDATE_EVENT_FAILED,
        error: res.message,
      });
    } else {
      yield put({
        type: TypeActions.UPDATE_EVENT_SUCCESS,
        data: res.data.results,
      });
      callback && callback.success();
    }
  } catch (error) {
    yield put({ type: TypeActions.UPDATE_EVENT_FAILED, error });
    callback && callback.failed(error.response.data.message);
  }
}

export default function* eventSaga() {
  yield takeLatest(TypeActions.GET_LIST_EVENT_REQUEST, getListEvent);
  yield takeLatest(TypeActions.CREATE_EVENT_REQUEST, createEvent);
  yield takeLatest(TypeActions.UPDATE_EVENT_REQUEST, updateEvent);
  yield takeLatest(TypeActions.DELETE_EVENT_REQUEST, deleteEvent);
  yield takeLatest(TypeActions.GET_EVENT_REQUEST, getEvent);
}
