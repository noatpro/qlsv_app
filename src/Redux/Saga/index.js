import { all, fork } from "redux-saga/effects";
import userSaga from "./userSaga";
import doorSaga from "./doorSaga";
import eventSaga from "./eventSaga";
import uploadSaga from "./uploadSaga";
import teamSaga from "./teamSaga";
import eventJoinSaga from "./eventJoinSaga";
import notificationSaga from "./notificationSaga";

export function* rootSagas() {
  yield all([
    fork(userSaga),
    fork(eventSaga),
    fork(doorSaga),
    fork(uploadSaga),
    fork(teamSaga),
    fork(eventJoinSaga),
    fork(notificationSaga),
  ]);
}
export default rootSagas;
