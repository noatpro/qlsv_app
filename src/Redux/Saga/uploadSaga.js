import _ from "lodash";
import { call, put, takeLatest } from "redux-saga/effects";
import TypeActions from "../TypeActions/index";
import ServiceURL from "../../Service/ServiceURL";
import { DELETE, GET, PATCH, POST } from "../../Service/ServiceBase";

export function* ImageUpload(data) {
  const url = ServiceURL.Upload;
  const callback = data.callback;
  try {
    const res = yield call(POST, url, data.body);
    console.log({ res });
    if (res.message && !_.isEmpty(res.message)) {
      yield put({
        type: TypeActions.UPLOAD_IMAGE_FAILED,
        error: res.message,
      });
      callback && callback.failed(res.message);
    } else {
      yield put({
        type: TypeActions.UPLOAD_IMAGE_SUCCESS,
        data: res.data.path,
      });
      callback && callback.success(res.data.path);
    }
  } catch (error) {
    yield put({ type: TypeActions.UPLOAD_IMAGE_FAILED, error });
    callback && callback.failed(error.response.data.message);
  }
}

export default function* uploadSaga() {
  yield takeLatest(TypeActions.UPLOAD_IMAGE_REQUEST, ImageUpload);
}
