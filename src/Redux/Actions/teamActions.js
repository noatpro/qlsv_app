// import TypeActions from "Redux/TypeActions";

import TypeActions from "../TypeActions";

export const getListTeam = (params) => {
  return {
    type: TypeActions.GET_LIST_TEAM_REQUEST,
    params,
  };
};

export const createTeam = (body, callback) => {
  return {
    type: TypeActions.CREATE_TEAM_REQUEST,
    body,
    callback,
  };
};

export const updateTeam = (body, params, callback) => {
  return {
    type: TypeActions.UPDATE_TEAM_REQUEST,
    body,
    params,
    callback,
  };
};

export const deleteTeam = (params, callback) => {
  return {
    type: TypeActions.DELETE_TEAM_REQUEST,
    params,
    callback,
  };
};

export default { getListTeam, createTeam, updateTeam, deleteTeam };
