import TypeActions from "Redux/TypeActions";

export const getDoor = (body, callback) => {
  return {
    type: TypeActions.GET_DOOR_REQUEST,
    body,
    callback,
  };
};

export const drawDoor = (body) => {
  return {
    type: TypeActions.DRAW_DOOR_REQUEST,
    body,
  };
};

export default { getDoor, drawDoor };
