import TypeActions from "../TypeActions/index";

export const login = (body, callback) => {
  return {
    type: TypeActions.LOGIN_REQUEST,
    body,
    callback,
  };
};

export const logout = (body, callback) => {
  return {
    type: TypeActions.LOG_OUT,
    body,
    callback,
  };
};

export const getUsers = (params) => {
  return {
    type: TypeActions.GET_USERS_REQUEST,
    params,
    // callback,
  };
};

export const deleteUsers = (params, callback) => {
  return {
    type: TypeActions.DELETE_USERS_REQUEST,
    params,
    callback,
  };
};

export const getListUsers = (params) => {
  return {
    type: TypeActions.GET_LIST_USERS_REQUEST,
    params,
  };
};

export const createUsers = (body, callback) => {
  return {
    type: TypeActions.CREATE_USERS_REQUEST,
    body,
    callback,
  };
};

export const changePassword = (body, params, callback) => {
  return {
    type: TypeActions.CHANGE_PASSWORD_REQUEST,
    body,
    params,
    callback,
  };
};
export const forgotPassword = (body, callback) => {
  return {
    type: TypeActions.FORGOT_PASSWORD_REQUEST,
    body,
    callback,
  };
};

export const editUsers = (body, params, callback) => {
  return {
    type: TypeActions.EDIT_USERS_REQUEST,
    body,
    params,
    callback,
  };
};

export default {
  login,
  getUsers,
  getListUsers,
  createUsers,
  logout,
  editUsers,
  deleteUsers,
  changePassword,
  forgotPassword,
};
