import TypeActions from "../TypeActions";

const initialState = {
  listNotification: [],
  isGetting: false,
  isCreating: false,
  error: "",
};

export const notificationReducer = (state = initialState, action) => {
  switch (action.type) {
    //!Get list
    case TypeActions.GET_LIST_NOTIFICATION_REQUEST:
      return {
        ...state,
        isGetting: true,
      };
    case TypeActions.GET_LIST_NOTIFICATION_SUCCESS:
      return {
        ...state,
        listNotification: action.data,
        isGetting: false,
      };
    case TypeActions.GET_LIST_NOTIFICATION_FAILED:
      return {
        ...state,
        isGetting: false,
      };
    //!Create
    case TypeActions.CREATE_NOTIFICATION_REQUEST:
      return {
        ...state,
        isCreating: true,
      };
    case TypeActions.CREATE_NOTIFICATION_SUCCESS:
      return {
        ...state,
        listNotification: action.data,
        isCreating: false,
      };
    case TypeActions.CREATE_NOTIFICATION_FAILED:
      return {
        ...state,
        isCreating: false,
      };
    default:
      return { ...state };
  }
};

export default notificationReducer;
