import TypeActions from "../TypeActions/index";

const initialState = {
  isUploading: false,
  imageLink: "",
  error: "",
};

export const uploadReducer = (state = initialState, action) => {
  switch (action.type) {
    //!Get Door
    case TypeActions.UPLOAD_IMAGE_REQUEST:
      return {
        ...state,
        isUploading: true,
      };
    case TypeActions.UPLOAD_IMAGE_SUCCESS:
      return {
        ...state,
        isUploading: false,
        imageLink: action.data,
      };
    case TypeActions.UPLOAD_IMAGE_FAILED:
      return {
        ...state,
        isUploading: false,
        error: action.error,
      };

    //!Default
    default:
      return {
        ...state,
      };
  }
};
export default uploadReducer;
