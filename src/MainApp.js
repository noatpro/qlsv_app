import { NavigationContainer } from "@react-navigation/native";
import _ from "lodash";
import React from "react";
import { SafeAreaView } from "react-native-safe-area-context";
import { useSelector } from "react-redux";
// import { SafeAreaView } from "react-native";
import Navigation from "./Navigations/Navigation";
import UnAuNavigation from "./Navigations/UnAuNavigation";

const MainApp = () => {
  // const detail = useSelector((RootState) => state.userReducer.data);
  // const a = new RootState();
  const detail = useSelector((state) => state.userReducer);
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "white" }}>
      <NavigationContainer>
        {!_.isEmpty(detail) && !_.isEmpty(detail.data) ? (
          <Navigation />
        ) : (
          <UnAuNavigation />
        )}
      </NavigationContainer>
    </SafeAreaView>
  );
};

export default MainApp;
