import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
import Forgot from "../screens/ForgotPass";
import LogIn from "../screens/Login";
import Register from "../screens/Register";

const Stack = createStackNavigator();
const UnAuNavigation = () => {
  return (
    <Stack.Navigator mode="card" headerMode="none">
      <Stack.Screen name="Login" component={LogIn} />
      <Stack.Screen name="ForgotPass" component={Forgot} />
      <Stack.Screen name="Resgiter" component={Register} />
    </Stack.Navigator>
  );
};

export default UnAuNavigation;
