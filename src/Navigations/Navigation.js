import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import { createStackNavigator } from "@react-navigation/stack";
import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { Dimensions } from "react-native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import ChangePassword from "../screens/ChangePassword";
import EventDetailAdmin from "../screens/EventDetailAdmin";
import EventDetailUser from "../screens/EventDetailUser";
import Forgot from "../screens/ForgotPass";
import AllEvent from "../screens/Home/AllEvent";
import EventGoing from "../screens/Home/EventGoing";
import EventJoined from "../screens/Home/EventJoined";
import LogIn from "../screens/Login";
import Notification from "../screens/Notification";
import Profile from "../screens/Profile";
import Register from "../screens/Register";
import UpdateDetailUser from "../screens/UpdateDetailUser";
const { width1 } = Dimensions.get("screen");

const TabTop = createMaterialTopTabNavigator();
const topTapNav = () => {
  // const allEvent = React.useRef();
  // useScrollToTop(allEvent);
  return (
    <TabTop.Navigator tabBarOptions={{ scrollEnabled: true }} lazy={true}>
      <TabTop.Screen
        name="Event Going"
        component={EventGoing}
        options={{
          title: "Sự kiện đang diễn ra",
        }}
      />
      <TabTop.Screen
        name="Event Joined"
        component={EventJoined}
        options={{
          title: "Sự kiện đã đăng ký",
        }}
      />
      <TabTop.Screen
        name="All event"
        component={AllEvent}
        options={{
          title: "Tất cả sự kiện",
        }}
      />
    </TabTop.Navigator>
  );
};

const StackUser = createStackNavigator();
const UserNav = () => {
  return (
    <StackUser.Navigator headerMode={"none"}>
      <StackUser.Screen name="Profile" component={Profile} />
      <StackUser.Screen name="UpdateDetailUser" component={UpdateDetailUser} />
      <StackUser.Screen name="ChangePassword" component={ChangePassword} />
    </StackUser.Navigator>
  );
};

const Tab = createBottomTabNavigator();
const BottomTab = () => {
  return (
    <Tab.Navigator
      // lazy={false}
      lazy
      activeColor="#1A237E"
      keyboardHidesNavigationBar
      initialRouteName="HomeTab"
      inactiveColor="#979797"
      // barStyle={{
      //   // backgroundColor: "rgba(255, 255, 255, 0.9)",
      //   alignItems: "center",
      //   justifyContent: "center",
      //   // padding: "2px 0 2px 0",
      //   // width: width1,
      // }}
      tabBarOptions={{
        activeTintColor: "#1A237E",
        inactiveTintColor: "#979797",
        keyboardHidesTabBar: true,
        tabStyle: { paddingTop: 4 },
        labelStyle: {
          paddingBottom: 4,
        },
      }}
    >
      <Tab.Screen
        name="AccountTab"
        component={Profile}
        options={{
          tabBarLabel: "Tài khoản",
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons
              name="account"
              color={color}
              size={30}
              // style={{ paddingBottom: "0" }}
            />
            // <PersonIcon color={color} size={30} />
          ),
        }}
      />
      <Tab.Screen
        name="HomeTab"
        component={topTapNav}
        options={{
          tabBarLabel: "Trang chủ",
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="home" color={color} size={29} />
          ),
        }}
      />
      <Tab.Screen
        name="NotificationTab"
        component={Notification}
        options={{
          tabBarLabel: "Thông báo",
          tabBarIcon: ({ color }) => (
            <MaterialCommunityIcons name="bell" color={color} size={26} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

const Stack = createStackNavigator();
const Navigation = () => {
  return (
    <Stack.Navigator headerMode={"none"} initialRouteName="Home">
      <Stack.Screen name="Home" component={BottomTab} />
      <Stack.Screen name="EventDetailAdmin" component={EventDetailAdmin} />
      <Stack.Screen name="EventDetailUser" component={EventDetailUser} />
      <Stack.Screen name="UpdateDetailUser" component={UpdateDetailUser} />
      <Stack.Screen name="ChangePassword" component={ChangePassword} />
      {/* <Stack.Screen name="Login" component={LogIn} />
      <Stack.Screen name="ForgotPass" component={Forgot} />
      <Stack.Screen name="Resgiter" component={Register} /> */}
    </Stack.Navigator>
  );
};

export default Navigation;
