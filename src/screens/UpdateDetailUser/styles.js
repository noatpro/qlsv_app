import { theme } from "galio-framework";
import { Dimensions, Platform, StyleSheet } from "react-native";
import { materialTheme } from "../../../constants";
import { HeaderHeight } from "../../../constants/utils";
const { width } = Dimensions.get("window");

export default StyleSheet.create({
  signin: {
    marginTop: Platform.OS === "android" ? -HeaderHeight : 0,
    alignItems: "center",
    justifyContent: "center",
  },
  input: {
    width: width * 0.9,
    borderRadius: 0,
    borderBottomWidth: 1,
    borderBottomColor: materialTheme.COLORS.PLACEHOLDER,
  },
  inputActive: {
    borderBottomColor: "white",
  },
  imageHorizontal: {
    width: theme.SIZES.BASE * 6.25,
    margin: theme.SIZES.BASE / 2,
  },
  shadow: {
    shadowColor: "black",
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 4,
    shadowOpacity: 0.2,
    elevation: 2,
  },
  qty: {
    width: width * 0.9,
    // backgroundColor: materialTheme.COLORS.DEFAULT,
    paddingHorizontal: theme.SIZES.BASE,
    paddingTop: 10,
    paddingBottom: 11,
    borderRadius: 3,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 4,
    shadowOpacity: 1,
    alignSelf: "center",
    borderBottomWidth: 1,
    borderBottomColor: materialTheme.COLORS.PLACEHOLDER,
  },
  dropdown: {
    marginTop: -theme.SIZES.BASE * 1.3,
    // marginTop: -20,
    marginLeft: -theme.SIZES.BASE * 1.05,
    width: width * 0.9,
    // width: theme.SIZES.BASE * 6,
  },
});
