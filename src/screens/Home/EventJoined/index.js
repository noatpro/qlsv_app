import { useNavigation, useIsFocused } from "@react-navigation/core";
import { Block } from "galio-framework";
import React from "react";
import { RefreshControl, ScrollView, FlatList } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import Event from "../../../components/Event";
import eventJoinActions from "../../../Redux/Actions/eventJoinActions";
import styles from "./styles";
import queryString from "query-string";

const EventJoined = () => {
  //!Const
  const forcus = useIsFocused();
  const Navigation = useNavigation();
  const idUser = useSelector((state) => state.userReducer.data.user.id);
  const listEventJoin = useSelector(
    (state) => state.eventJoinReducer.listEventJoin
  );
  const isGetting = useSelector((state) => state.eventJoinReducer.isGetting);
  const user = useSelector((state) => state.userReducer.data.user);
  const dispatch = useDispatch();

  //!State
  const [eventDetail, seteventDetail] = React.useState([]);
  const [limit, setlimit] = React.useState(10);
  const [totalResults, settotalResults] = React.useState(0);
  const [detailParams, setdetailParams] = React.useState({
    userId: idUser,
    sortBy: "startTime:desc",
    populate: "eventId",
  });
  //!UseEffect
  // React.useEffect(() => {
  //   dispatch(
  //     eventJoinActions.getListEventJoin(
  //       queryString.stringify({
  //         userId: idUser,
  // sortBy: "startTime:desc",
  //         populate: "eventId",
  //       })
  //     )
  //   );
  // }, []);
  React.useEffect(() => {
    if (forcus) {
      dispatch(
        eventJoinActions.getListEventJoin(queryString.stringify(detailParams), {
          success: (data) => {
            settotalResults(data.totalResults);
          },
          failed: (mess) => {},
        })
      );
    }
  }, [forcus, detailParams]);
  React.useEffect(() => {
    seteventDetail(listEventJoin);
  }, [listEventJoin]);
  //!Function
  const onRefresh = React.useCallback(() => {
    // setTimeout(() => {
    //   setRefreshing(false);
    // }, 2000);
    dispatch(
      eventJoinActions.getListEventJoin(queryString.stringify(detailParams), {
        success: (data) => {
          settotalResults(data.totalResults);
        },
        failed: (mess) => {},
      })
    );
    // wait(2000).then(() => );
  }, []);
  return (
    <Block flex center style={styles.home}>
      {/* <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.event}
        refreshControl={
          <RefreshControl refreshing={isGetting} onRefresh={onRefresh} />
        }
      > */}
      <Block flex>
        <FlatList
          contentContainerStyle={styles.event}
          showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl refreshing={isGetting} onRefresh={onRefresh} />
          }
          data={eventDetail}
          // onEndReachedThreshold={100}
          keyExtractor={(item) => item.id}
          scrollEnabled={true}
          onEndReached={() => {
            if (eventDetail.length < totalResults) {
              setlimit(limit + 10);
              setdetailParams({
                ...detailParams,
                limit: limit + 10,
              });
            }
            // dispatch(eventActions.getListEvent(queryString.stringify(detailParams)));
          }}
          renderItem={(index) => {
            const eventDetail = {
              address: index.item.eventId.address,
              description: index.item.eventId.description,
              id: index.item.eventId.id,
              // idEventJoin: index.item.id,
              image: index.item.eventId.image,
              name: index.item.eventId.name,
              score: index.item.eventId.score,
              startTime: index.item.eventId.startTime,
              endTime: index.item.eventId.endTime,
              startTimeRegister: index.item.eventId.startTimeRegister,
              endTimeRegister: index.item.eventId.endTimeRegister,
              status: index.item.eventId.status + " - " + index.item.status,
              isOpenRegister: index.item.eventId.isOpenRegister,
            };
            return (
              <Event
                key={index.index}
                event={eventDetail}
                horizontal
                numberOfLines={2}
                onPress={() => {
                  user.role === "admin"
                    ? Navigation.navigate("EventDetailAdmin", eventDetail)
                    : Navigation.navigate("EventDetailUser", eventDetail);
                }}
                numberOfLines={2}
              />
            );
          }}
        />
        {/* {eventDetail.map((index, Key) => {
          const eventDetail = {
            address: index.eventId.address,
            description: index.eventId.description,
            id: index.eventId.id,
            image: index.eventId.image,
            name: index.eventId.name,
            score: index.eventId.score,
            startTime: index.eventId.startTime,
            endTime: index.eventId.endTime,
            startTimeRegister: index.eventId.startTimeRegister,
            endTimeRegister: index.eventId.endTimeRegister,
            status: index.status,
          };
          return (
            <Event
              key={Key}
              event={eventDetail}
              horizontal
              numberOfLines={2}
              onPress={() => {
                user.role === "admin"
                  ? Navigation.navigate("EventDetailAdmin", eventDetail)
                  : Navigation.navigate("EventDetailUser", eventDetail);
              }}
              numberOfLines={2}
            />
          );
        })} */}
      </Block>
      {/* </ScrollView> */}
      {/* <Text>1234</Text> */}
    </Block>
  );
};

export default EventJoined;
