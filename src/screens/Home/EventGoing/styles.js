import { theme } from "galio-framework";
import { Dimensions, StyleSheet } from "react-native";
const { width } = Dimensions.get("screen");

export default StyleSheet.create({
  home: {
    width: width,
    // flex: 1,
  },
  divider: {
    borderRightWidth: 0.3,
    borderRightColor: theme.COLORS.MUTED,
  },
  event: {
    width: width - theme.SIZES.BASE * 2,
    paddingVertical: theme.SIZES.BASE * 2,
  },
  //   search: {
  //     height: 48,
  //     width: width - 32,
  //     marginHorizontal: 16,
  //     borderWidth: 1,
  //     borderRadius: 3,
  //   },
  //   header: {
  //     backgroundColor: theme.COLORS.WHITE,
  //     shadowColor: theme.COLORS.BLACK,
  //     shadowOffset: {
  //       width: 0,
  //       height: 2,
  //     },
  //     shadowRadius: 8,
  //     shadowOpacity: 0.2,
  //     elevation: 4,
  //     zIndex: 2,
  //   },
  //   tabs: {
  //     marginBottom: 24,
  //     marginTop: 10,
  //     elevation: 4,
  //   },
  //   tab: {
  //     backgroundColor: theme.COLORS.TRANSPARENT,
  //     width: width * 0.5,
  //     borderRadius: 0,
  //     borderWidth: 0,
  //     height: 24,
  //     elevation: 0,
  //   },
  //   tabTitle: {
  //     lineHeight: 19,
  //     fontWeight: "300",
  //   },
});
