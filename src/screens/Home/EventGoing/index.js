import { useIsFocused, useNavigation } from "@react-navigation/core";
import { Block } from "galio-framework";
import React from "react";
import { RefreshControl, ScrollView, FlatList } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import Event from "../../../components/Event";
import eventActions from "../../../Redux/Actions/eventActions";
import eventJoinActions from "../../../Redux/Actions/eventJoinActions";
import styles from "./styles";
import queryString from "query-string";

const EventGoing = () => {
  //!Const
  const dispatch = useDispatch();
  const forcus = useIsFocused();
  const Navigation = useNavigation();
  const idUser = useSelector((state) => state.userReducer.data.user.id);
  const listEvent = useSelector((state) => state.eventReducer.listEvent);
  const user = useSelector((state) => state.userReducer.data.user);
  const isGetting = useSelector((state) => state.eventReducer.isGetting);

  //!State
  const [eventDetail, seteventDetail] = React.useState([]);
  const [limit, setlimit] = React.useState(10);
  const [totalResults, settotalResults] = React.useState(0);
  const [detailParams, setdetailParams] = React.useState({
    sortBy: "startTime:desc",
    limit: limit,
  });
  //!Useeffect
  // React.useEffect(() => {
  //   dispatch(eventActions.getListEvent(""));
  // }, []);
  React.useEffect(() => {
    if (forcus) {
      dispatch(
        eventActions.getListEvent(queryString.stringify(detailParams), {
          success: (data) => {
            settotalResults(data.totalResults);
          },
          failed: (mess) => {},
        })
      );
    }
  }, [forcus, detailParams]);

  React.useEffect(() => {
    seteventDetail(listEvent);
  }, [listEvent]);
  //!Function
  const onRefresh = React.useCallback(() => {
    // setRefreshing(true);
    dispatch(
      eventActions.getListEvent(queryString.stringify(detailParams), {
        success: (data) => {
          settotalResults(data.totalResults);
        },
      })
    );
    // setTimeout(() => {
    //   setRefreshing(false);
    // }, 2000);
    // wait(2000).then(() => );
  }, []);
  return (
    <Block flex center style={styles.home}>
      {/* <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.event}
        refreshControl={
          <RefreshControl refreshing={isGetting} onRefresh={onRefresh} />
        }
      > */}
      <Block flex>
        <FlatList
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.event}
          scrollEnabled={true}
          data={eventDetail}
          refreshControl={
            <RefreshControl refreshing={isGetting} onRefresh={onRefresh} />
          }
          onEndReached={() => {
            if (eventDetail.length < totalResults) {
              setlimit(limit + 10);
              setdetailParams({
                ...detailParams,
                limit: limit + 10,
              });
            }
          }}
          // onEndReachedThreshold={100}
          renderItem={(index) => {
            if (index.item.status !== "Đã diễn ra") {
              //  else {
              return (
                <Event
                  key={index.index}
                  event={index.item}
                  horizontal
                  onPress={() => {
                    user.role === "admin"
                      ? Navigation.navigate("EventDetailAdmin", index.item)
                      : Navigation.navigate("EventDetailUser", index.item);
                  }}
                  numberOfLines={2}
                />
              );
            }
          }}
        />
        {/* {eventDetail.map((index, Key) => {
          if (index.status !== "Đã diễn ra") {
            //  else {
            return (
              <Event
                key={Key}
                event={index}
                horizontal
                onPress={() => {
                  user.role === "admin"
                    ? Navigation.navigate("EventDetailAdmin", index)
                    : Navigation.navigate("EventDetailUser", index);
                }}
                numberOfLines={2}
              />
            );
          }
        })} */}
      </Block>
      {/* </ScrollView> */}
      {/* <Text>1234</Text> */}
    </Block>
  );
};

export default EventGoing;
