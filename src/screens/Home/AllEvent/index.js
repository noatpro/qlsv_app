import { useNavigation, useIsFocused } from "@react-navigation/core";
import { Block } from "galio-framework";
import React from "react";
import { RefreshControl, ScrollView, FlatList } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import Event from "../../../components/Event";
import eventActions from "../../../Redux/Actions/eventActions";
import * as Notifications from "expo-notifications";
import styles from "./styles";
import queryString from "query-string";

const AllEvent = () => {
  //!Const
  const forcus = useIsFocused();
  const Navigation = useNavigation();
  const dispatch = useDispatch();
  const listEvent = useSelector((state) => state.eventReducer.listEvent);
  const isGetting = useSelector((state) => state.eventReducer.isGetting);
  const user = useSelector((state) => state.userReducer.data.user);

  //!State
  const [eventDetail, seteventDetail] = React.useState([]);
  const [limit, setlimit] = React.useState(10);
  const [totalResults, settotalResults] = React.useState(0);
  const [detailParams, setdetailParams] = React.useState({
    sortBy: "startTime:desc",
    limit: limit,
  });
  //!Use effect
  // React.useEffect(() => {
  //   dispatch(eventActions.getListEvent( queryString.stringify({ sortBy: "startTime:desc" })));
  // }, []);
  React.useEffect(() => {
    if (forcus) {
      dispatch(
        eventActions.getListEvent(queryString.stringify(detailParams), {
          success: (data) => {
            settotalResults(data.totalResults);
          },
          failed: (mess) => {},
        })
      );
    }
  }, [forcus, detailParams]);
  React.useEffect(() => {
    seteventDetail(listEvent);
  }, [listEvent]);
  //!Function
  const onRefresh = React.useCallback(() => {
    // setRefreshing(true);
    // setTimeout(() => {
    //   setRefreshing(false);
    // }, 2000);
    dispatch(
      eventActions.getListEvent(queryString.stringify(detailParams), {
        success: (data) => {
          settotalResults(data.totalResults);
        },
        failed: (mess) => {},
      })
    );

    // wait(2000).then(() => );
  }, []);
  return (
    <Block flex center style={styles.home}>
      {/* <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.event}
        refreshControl={
          <RefreshControl refreshing={isGetting} onRefresh={onRefresh} />
        }
      > */}
      <Block flex>
        {/* {eventDetail.map((index, Key) => {
            return (
              <Event
                key={Key}
                event={index}
                onPress={() => {
                  user.role === "admin"
                    ? Navigation.navigate("EventDetailAdmin", index)
                    : Navigation.navigate("EventDetailUser", index);
                }}
                horizontal
                numberOfLines={2}
              />
            );
          })} */}
        <FlatList
          contentContainerStyle={styles.event}
          showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl refreshing={isGetting} onRefresh={onRefresh} />
          }
          data={eventDetail}
          renderItem={(index) => {
            return (
              <Event
                key={index.index}
                event={index.item}
                onPress={() => {
                  user.role === "admin"
                    ? Navigation.navigate("EventDetailAdmin", index.item)
                    : Navigation.navigate("EventDetailUser", index.item);
                }}
                horizontal
                numberOfLines={2}
              />
            );
          }}
          keyExtractor={(item) => item.id}
          // scrollEnabled={false}
          onEndReached={() => {
            if (eventDetail.length < totalResults) {
              setlimit(limit + 10);
              setdetailParams({
                ...detailParams,
                limit: limit + 10,
              });
            }
            // dispatch(eventActions.getListEvent(queryString.stringify(detailParams)));
          }}
          // onEndReachedThreshold={0}
        />
      </Block>
      {/* </ScrollView> */}
      {/* <Text>1234</Text> */}
    </Block>
  );
};

export default AllEvent;
