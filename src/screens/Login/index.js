import { useNavigation } from "@react-navigation/core";
import { LinearGradient } from "expo-linear-gradient";
import { Formik } from "formik";
import { Block, Button, Input, Text, theme } from "galio-framework";
import React from "react";
import { Alert, Image, KeyboardAvoidingView } from "react-native";
import AnimatedLoader from "react-native-animated-loader";
import { ScrollView } from "react-native-gesture-handler";
import { useDispatch } from "react-redux";
import * as yup from "yup";
import { materialTheme } from "../../../constants";
import userActions from "../../Redux/Actions/userActions";
import styles from "./styles";

const LogIn = () => {
  //!Const
  const dispatch = useDispatch();
  const Navigation = useNavigation();
  const [LogIn, setLogIn] = React.useState({
    studentCode: "",
    password: "",
    studentCodeActive: false,
    passwordActive: false,
  });
  const logSchema = yup.object().shape({
    studentCode: yup
      .string()
      .min(10, ({ min }) => "Mã sinh viên phải có 10 kí tự")
      .max(10, ({ max }) => "Mã sinh viên phải có 10 kí tự")
      .required("Vui lòng nhập mã sinh viên!"),
    password: yup
      .string()
      .required("Vui lòng nhập mật khẩu!")
      .min(8, ({ min }) => "Mật khẩu phải có ít nhất 8 ký tự!"),
  });
  //!useState
  const [loading, setloading] = React.useState(false);
  //!Function
  const handleSubmit = (value) => {
    setloading(true);
    const body = { studentCode: value.studentCode, password: value.password };
    dispatch(
      userActions.login(body, {
        success: () => {
          setloading(false);
        },
        failed: (mess) => {
          Alert.alert("Đăng nhập thất bại! Lỗi: " + mess + "!");
          setloading(false);
        },
      })
    );
    // setTimeout(() => {
    //   setloading(false);
    //   Alert.alert(
    //     "Sign in action",
    //     `studentCode: ${value.studentCode} Password: ${value.password}`
    //   );
    //   Navigation.navigate("Home");
    // }, 2000);
    // }
  };
  return (
    <LinearGradient
      start={{ x: 0, y: 0 }}
      end={{ x: 0.25, y: 1.1 }}
      locations={[0.2, 1]}
      // colors={["#7986CB", "#1A237E"]}
      colors={["#7986CB", "#1A237E"]}
      style={[styles.signin, { flex: 1, paddingTop: theme.SIZES.BASE * 4 }]}
    >
      <Block flex middle style={{ flex: 1 }}>
        <KeyboardAvoidingView
          behavior="height"
          enabled
          style={{ flex: 1, alignContent: "center", justifyContent: "center" }}
        >
          <ScrollView
            style={{
              flex: 1,
              // alignContent: "center",
              paddingTop: theme.SIZES.BASE * 5,
            }}
            // contentContainerStyle={{ justifyContent: "flex-end" }}
          >
            <AnimatedLoader
              visible={loading}
              overlayColor="rgba(255,255,255,0.75)"
              source={require("../../9764-loader.json")}
              animationStyle={{ width: 100, height: 100 }}
              speed={1}
            >
              <Text style={{ fontSize: 18, fontWeight: "bold" }}>
                Xin hãy đợi...
              </Text>
            </AnimatedLoader>
            <Block
              block
              bottom
              space="between"
              style={{
                marginVertical: theme.SIZES.BASE * 1.875,
                // justifyContent: "flex-end",
              }}
            >
              <Block
                block
                center
                space="between"
                style={{
                  marginVertical: theme.SIZES.BASE * 1.875,
                  flex: 2,
                  justifyContent: "center",
                  //
                }}
              >
                <Block center>
                  <Image
                    source={require("../../img/Logo.png")}
                    style={{
                      height: theme.SIZES.BASE * 10,
                      width: theme.SIZES.BASE * 10,
                      marginTop: -theme.SIZES.BASE,
                      opacity: 0.7,
                      borderRadius: 3,
                    }}
                  />
                </Block>
              </Block>
              <Formik
                validationSchema={logSchema}
                enableReinitialize
                initialValues={LogIn}
                onSubmit={handleSubmit}
              >
                {({ values, setFieldValue, handleSubmit, errors, touched }) => {
                  return (
                    <Block block style={{ flex: 1 }}>
                      <Block
                        center
                        style={{
                          justifyContent: "center",
                          marginTop: 20,
                        }}
                      >
                        <Block>
                          <Input
                            borderless
                            color="white"
                            placeholder="Mã sinh viên..."
                            type="numeric"
                            value={values.studentCode}
                            autoCapitalize="none"
                            bgColor="transparent"
                            onBlur={() => {
                              const clone = { ...LogIn };
                              // setLogIn({
                              //   ...LogIn,
                              //   studentCodeActive: !clone.studentCodeActive,
                              // });
                              setFieldValue(
                                "studentCodeActive",
                                !values.studentCodeActive
                              );
                            }}
                            onFocus={() => {
                              setFieldValue(
                                "studentCodeActive",
                                !values.studentCodeActive
                              );
                            }}
                            placeholderTextColor={
                              materialTheme.COLORS.PLACEHOLDER
                            }
                            onChangeText={(text) => {
                              setFieldValue("studentCode", text);
                            }}
                            style={[
                              styles.input,
                              values.studentCodeActive
                                ? styles.inputActive
                                : null,
                            ]}
                          />
                          {errors.studentCode && touched.studentCode && (
                            <Text style={{ fontSize: 12, color: "red" }}>
                              {errors.studentCode}
                            </Text>
                          )}
                          <Input
                            password
                            viewPass
                            value={values.password}
                            borderless
                            color="white"
                            iconColor="white"
                            placeholder="Mật khẩu..."
                            bgColor="transparent"
                            onBlur={() => {
                              setFieldValue(
                                "passwordActive",
                                !values.passwordActive
                              );
                            }}
                            onFocus={() => {
                              setFieldValue(
                                "passwordActive",
                                !values.passwordActive
                              );
                            }}
                            placeholderTextColor={
                              materialTheme.COLORS.PLACEHOLDER
                            }
                            onChangeText={(text) => {
                              // setLogIn({ ...LogIn, password: text });
                              setFieldValue("password", text);
                            }}
                            style={[
                              styles.input,
                              values.passwordActive ? styles.inputActive : null,
                            ]}
                          />
                          {errors.password && touched.password && (
                            <Text style={{ fontSize: 12, color: "red" }}>
                              {errors.password}
                            </Text>
                          )}
                        </Block>

                        <Text
                          color={theme.COLORS.WHITE}
                          size={theme.SIZES.FONT * 0.75}
                          onPress={() => {
                            Navigation.navigate("ForgotPass");
                            // Navigation.canGoBack();
                          }}
                          style={{
                            alignSelf: "flex-end",
                            lineHeight: theme.SIZES.FONT * 2,
                          }}
                        >
                          Quên mật khẩu?
                        </Text>
                      </Block>
                      <Block center>
                        <Block center flex style={{ marginTop: 20 }}>
                          <Button
                            size="large"
                            shadowless
                            color={materialTheme.COLORS.MICHAEL_COLOR}
                            style={{ height: 48 }}
                            onPress={handleSubmit}
                          >
                            ĐĂNG NHẬP
                          </Button>
                          {/* <Button
                            size="large"
                            color="transparent"
                            shadowless
                            onPress={() => {
                              Navigation.navigate("Resgiter");
                            }}
                          >
                            <Text
                              center
                              color={theme.COLORS.WHITE}
                              size={theme.SIZES.FONT * 0.75}
                              style={{ marginTop: 20 }}
                            >
                              {"Chưa có tài khoản? Đăng ký"}
                            </Text>
                          </Button> */}
                        </Block>
                      </Block>
                    </Block>
                  );
                }}
              </Formik>
            </Block>
          </ScrollView>
        </KeyboardAvoidingView>
      </Block>
      <Block
        block
        style={{
          // backgroundColor: "red",
          paddingBottom: theme.SIZES.BASE,
        }}
      >
        <Text style={{ color: theme.COLORS.WHITE, fontSize: 13 }}>
          2021, Created by Nguyễn Tuấn - 2017603477
        </Text>
      </Block>
    </LinearGradient>
  );
};

export default LogIn;
