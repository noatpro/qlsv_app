import { LinearGradient } from "expo-linear-gradient";
import { Block, Button, Input, Text, theme } from "galio-framework";
import React from "react";
import { Alert, Image, KeyboardAvoidingView } from "react-native";
import { materialTheme } from "../../../constants";
import styles from "./styles";
import { ScrollView } from "react-native-gesture-handler";
import * as yup from "yup";
import { Formik } from "formik";
import { useNavigation } from "@react-navigation/core";
import { useDispatch } from "react-redux";
import userActions from "../../Redux/Actions/userActions";

const Forgot = () => {
  const Navigation = useNavigation();
  const dispatch = useDispatch();
  const [forgot, setforgot] = React.useState({
    studentCode: "",
    studentCodeActive: false,
  });
  const forgotSchema = yup.object().shape({
    studentCode: yup
      .string()
      .min(10, ({ min }) => `Mã sinh viên phải có ${min} ký tự`)
      .max(10, ({ max }) => `Mã sinh viên phải có ${max} ký tự`)
      .required("Vui lòng nhập mã sinh viên!"),
  });
  const handleForgot = (value) => {
    const forgot = { studentCode: value.studentCode };
    dispatch(
      userActions.forgotPassword(forgot, {
        success: () => {
          Alert.alert(
            "Thông báo!",
            "Link cấp lại mật khẩu đã được gửi tới email của bạn! Vui lòng kiểm tra Email!"
          );
          Navigation.goBack();
        },
        failed: (mess) => {
          Alert.alert(
            "Thông báo!",
            "Cấp lại mật khẩu thất bại! Lỗi: " + mess + "!"
          );
        },
      })
    );
  };
  return (
    <LinearGradient
      start={{ x: 0, y: 0 }}
      end={{ x: 0.25, y: 1.1 }}
      locations={[0.2, 1]}
      colors={["#7986CB", "#1A237E"]}
      style={[styles.forgot, { flex: 1, paddingTop: theme.SIZES.BASE * 4 }]}
    >
      <Block flex middle style={{ flex: 1 }}>
        <KeyboardAvoidingView
          behavior="height"
          enabled
          style={{ flex: 1, alignContent: "center", justifyContent: "center" }}
        >
          <ScrollView
            style={{
              flex: 1,

              paddingTop: theme.SIZES.BASE * 5,
            }}
          >
            <Block
              block
              bottom
              space="between"
              style={{
                marginVertical: theme.SIZES.BASE * 1.875,
              }}
            >
              <Block
                block
                center
                space="between"
                style={{
                  marginVertical: theme.SIZES.BASE * 1.875,
                  flex: 2,
                  justifyContent: "center",
                  //
                }}
              >
                <Block center>
                  <Image
                    source={require("../../img/Logo.png")}
                    style={{
                      height: theme.SIZES.BASE * 10,
                      width: theme.SIZES.BASE * 10,
                      marginTop: -theme.SIZES.BASE,
                      opacity: 0.7,
                      borderRadius: 3,
                    }}
                  />
                </Block>
              </Block>
              <Formik
                validationSchema={forgotSchema}
                enableReinitialize
                initialValues={forgot}
                onSubmit={handleForgot}
              >
                {({ values, setFieldValue, handleSubmit, errors, touched }) => {
                  return (
                    <Block block style={{ flex: 1 }}>
                      <Block
                        center
                        style={{
                          justifyContent: "center",
                          marginTop: 20,
                        }}
                      >
                        <Block>
                          <Input
                            borderless
                            color="white"
                            placeholder="Mã sinh viên..."
                            type="numeric"
                            value={values.studentCode}
                            autoCapitalize="none"
                            bgColor="transparent"
                            onBlur={() => {
                              setFieldValue(
                                "studentCodeActive",
                                !values.studentCodeActive
                              );
                            }}
                            onFocus={() => {
                              setFieldValue(
                                "studentCodeActive",
                                !values.studentCodeActive
                              );
                            }}
                            placeholderTextColor={
                              materialTheme.COLORS.PLACEHOLDER
                            }
                            onChangeText={(text) => {
                              setFieldValue("studentCode", text);
                            }}
                            style={[
                              styles.input,
                              values.studentCodeActive
                                ? styles.inputActive
                                : null,
                            ]}
                          />
                          {errors.studentCode && touched.studentCode && (
                            <Text style={{ fontSize: 12, color: "red" }}>
                              {errors.studentCode}
                            </Text>
                          )}
                        </Block>

                        {/* <Text
                          color={theme.COLORS.WHITE}
                          size={theme.SIZES.FONT * 0.75}
                          onPress={() => Navigation.navigate("Resgiter")}
                          style={{
                            alignSelf: "flex-end",
                            lineHeight: theme.SIZES.FONT * 2,
                          }}
                        >
                          Chưa có tài khoản? Đăng ký
                        </Text> */}
                      </Block>
                      <Block center>
                        <Block center flex style={{ marginTop: 20 }}>
                          <Button
                            size="large"
                            shadowless
                            color={materialTheme.COLORS.MICHAEL_COLOR}
                            style={{ height: 48 }}
                            onPress={handleSubmit}
                          >
                            CẤP LẠI MẬT KHẨU
                          </Button>
                          <Button
                            size="large"
                            color="transparent"
                            shadowless
                            onPress={() => {
                              Navigation.goBack();
                            }}
                          >
                            <Text
                              center
                              color={theme.COLORS.WHITE}
                              size={theme.SIZES.FONT * 0.75}
                              style={{ marginTop: 20 }}
                            >
                              {"Đã có tài khoản? Đăng nhập"}
                            </Text>
                          </Button>
                        </Block>
                      </Block>
                    </Block>
                  );
                }}
              </Formik>
            </Block>
          </ScrollView>
        </KeyboardAvoidingView>
      </Block>
      <Block
        block
        style={{
          // backgroundColor: "red",
          paddingBottom: theme.SIZES.BASE,
        }}
      >
        <Text style={{ color: theme.COLORS.WHITE, fontSize: 13 }}>
          2021, Created by Nguyễn Tuấn - 2017603477
        </Text>
      </Block>
    </LinearGradient>
  );
};

export default Forgot;
