import { useIsFocused, useNavigation } from "@react-navigation/core";
import { Block, Text } from "galio-framework";
import React from "react";
import { Dimensions, RefreshControl, FlatList } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import { useDispatch, useSelector } from "react-redux";
import CustomNotifications from "../../components/CustomNotifications";
import eventActions from "../../Redux/Actions/eventActions";
import notificationActions from "../../Redux/Actions/notificationActions";
import styles from "./styles";
import queryString from "query-string";
const { width } = Dimensions.get("screen");

const Notification = () => {
  //!Const
  const Navigation = useNavigation();
  const listNotification = useSelector(
    (state) => state.notificationReducer.listNotification
  );
  const user = useSelector((state) => state.userReducer.data.user);
  const isGetting = useSelector((state) => state.notificationReducer.isGetting);

  const dispatch = useDispatch();
  const forcus = useIsFocused();
  //!UseState
  const [notification, setnotification] = React.useState([]);
  const [limit, setlimit] = React.useState(10);
  const [totalResults, settotalResults] = React.useState(0);
  const [detailParams, setdetailParams] = React.useState({
    sortBy: "time:desc",
    limit: limit,
  });
  //!UseEffect

  React.useEffect(() => {
    if (forcus) {
      dispatch(
        notificationActions.getListNotification(
          queryString.stringify(detailParams),
          {
            success: (data) => {
              settotalResults(data.totalResults);
            },
            failed: (mess) => {},
          }
        )
      );
    }
  }, [forcus, detailParams]);
  React.useEffect(() => {
    if (listNotification && listNotification !== undefined)
      setnotification(listNotification);
  }, [listNotification]);
  //!Function
  const onRefresh = () => {
    dispatch(
      notificationActions.getListNotification(
        queryString.stringify(detailParams),
        {
          success: (data) => {
            settotalResults(data.totalResults);
          },
          failed: (mess) => {},
        }
      )
    );
  };
  return (
    <Block flex center style={styles.home}>
      {/* <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.notification}
        refreshControl={
          <RefreshControl refreshing={isGetting} onRefresh={onRefresh} />
        }
      > */}
      <FlatList
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.notification}
        refreshControl={
          <RefreshControl refreshing={isGetting} onRefresh={onRefresh} />
        }
        data={notification}
        renderItem={(index) => {
          return (
            <CustomNotifications
              notification={index.item}
              key={index.index}
              onPress={() => {
                dispatch(
                  eventActions.getEvent(index.item.eventId, {
                    success: (data) => {
                      user.role === "admin"
                        ? Navigation.navigate("EventDetailAdmin", data)
                        : Navigation.navigate("EventDetailUser", data);
                    },
                    failed: (mess) => {
                      console.log({ mess });
                    },
                  })
                );
              }}
            />
          );
        }}
        keyExtractor={(item) => item.id}
        onEndReached={() => {

          if (notification.length < totalResults) {
            setlimit(limit + 10);
            setdetailParams({
              ...detailParams,
              limit: limit + 10,
            });
          }
        }}
      />
      {/* </ScrollView> */}
    </Block>
  );
};

export default Notification;
