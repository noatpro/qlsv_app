import { theme } from "galio-framework";
import { Dimensions, StyleSheet } from "react-native";
const { width } = Dimensions.get("screen");

export default StyleSheet.create({
  home: {
    width: width,
    // flex: 1,
  },
  divider: {
    borderRightWidth: 0.3,
    borderRightColor: theme.COLORS.MUTED,
  },
  notification: {
    width: width - theme.SIZES.BASE * 2,
    paddingVertical: theme.SIZES.BASE * 2,
  },
});
