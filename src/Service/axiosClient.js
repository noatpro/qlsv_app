import axios from "axios";
import queryString from "query-string";
import { BASE_URL } from "./ServiceURL";

const axiosClient = axios.create({
  baseURL: BASE_URL,
  timeout: 5000,
  paramsSerializer: (params) => queryString.stringify(params),
});

const setToken = (token) => {
  return axiosClient.interceptors.request.use(
    async (config) => {
      config.headers.authorization = `Bearer ${token}`;
      // config.headers.authorization = `Bearer ${localStorage.getItem("token")}`;
      // const expireAt = new Date(localStorage.getItem("expiresAt"));
      // let token = localStorage.getItem("token");
      // if (token && !_.isEmpty(token))
      //   if (new Date().getTime() > expireAt.getTime()) {
      //     const data = refreshToken();
      //     token = typeof data === "string" ? data : await data;
      //   }
      // // setting updated token
      // localStorage.setItem("token", token);
      return config;
    },
    (error) => {
      return Promise.reject(error);
    }
  );
};

const removeAuth = () => {
  return axiosClient.interceptors.request.use(
    async (config) => {
      config.headers.authorization = ``;
      return config;
    },
    (error) => {
      return Promise.reject(error);
    }
  );
};

// const axiosClientPdf = axios.create({
//   baseURL: BASE_URL,
//   timeout: 5000,
//   paramsSerializer: (params) => queryString.stringify(params),
// });

// axiosClientPdf.interceptors.request.use(
//   async (config) => {
//     config.headers.authorization = `Bearer ${localStorage.getItem("token")}`;
//     const expireAt = new Date(localStorage.getItem("expiresAt"));
//     let token = localStorage.getItem("token");
//     if (token && !_.isEmpty(token))
//       if (new Date().getTime() > expireAt.getTime()) {
//         const data = refreshToken();
//         token = typeof data === "string" ? data : await data;
//       }
//     // setting updated token
//     localStorage.setItem("token", token);
//     config.responseType = "blob";
//     return config;
//   },
//   (error) => {
//     return Promise.reject(error);
//   }
// );

// export default {axiosClient, axiosClientPdf};

const EXPO_SERVER_URL = "https://exp.host/--/api/v2/push/send";
const pushNotification = async (token, title, body) => {
  const message = {
    to: token,
    sound: "default",
    title: title,
    body: body,
  };
  await axios.post(EXPO_SERVER_URL, message);
};

export { axiosClient, setToken, removeAuth, pushNotification };
