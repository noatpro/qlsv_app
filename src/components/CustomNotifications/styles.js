import { theme } from "galio-framework";
import { Dimensions, StyleSheet } from "react-native";
const { width } = Dimensions.get("screen");

export default StyleSheet.create({
  notification: {
    backgroundColor: theme.COLORS.WHITE,
    marginVertical: theme.SIZES.BASE / 3,
    borderWidth: 0,
    minHeight: 70,
    alignItems: "center",
  },
  notificationTitle: {
    flexWrap: "wrap",
    paddingBottom: 6,
    justifyContent: "center",
  },
  notificationDescription: {
    padding: theme.SIZES.BASE / 2,

    justifyContent: "center",
    alignContent: "center",
  },
  shadow: {
    shadowColor: theme.COLORS.BLACK,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 3,
    shadowOpacity: 0.1,
    elevation: 2,
  },
});
