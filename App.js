import React from "react";
import { Platform, StatusBar, Image, SafeAreaView, Alert } from "react-native";
import AppLoading from "expo-app-loading";
import { Asset } from "expo-asset";
import { Block, GalioProvider } from "galio-framework";
import { NavigationContainer } from "@react-navigation/native";
import Login from "./src/screens/Login";
import { LogBox } from "react-native";
import * as Notifications from "expo-notifications";
// Before rendering any navigation stack
import { enableScreens } from "react-native-screens";
enableScreens();

import Screens from "./navigation/Screens";
import { Images, materialTheme } from "./constants/";
import Register from "./src/screens/Register";
import Forgot from "./src/screens/ForgotPass";
import UnAuNavigation from "./src/Navigations/UnAuNavigation";
import Navigation from "./src/Navigations/Navigation";
import MainApp from "./src/MainApp";
import { Provider } from "react-redux";
import { store, persistor } from "./src/Store/index";
import { PersistGate } from "redux-persist/integration/react";
LogBox.ignoreLogs(["Remote debugger"]);

const assetImages = [
  Images.Profile,
  Images.Avatar,
  Images.Onboarding,
  Images.Products.Auto,
  Images.Products.Motocycle,
  Images.Products.Watches,
  Images.Products.Makeup,
  Images.Products.Accessories,
  Images.Products.Fragrance,
  Images.Products.BMW,
  Images.Products.Mustang,
  Images.Products["Harley-Davidson"],
];

// cache product images
// products.map(product => assetImages.push(product.image));

// cache categories images
// Object.keys(categories).map(key => {
//   categories[key].map(category => assetImages.push(category.image));
// });

function cacheImages(images) {
  return images.map((image) => {
    if (typeof image === "string") {
      return Image.prefetch(image);
    } else {
      return Asset.fromModule(image).downloadAsync();
    }
  });
}

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: true,
    shouldSetBadge: false,
  }),
});
export default class App extends React.Component {
  state = {
    isLoadingComplete: false,
  };
  render() {
    if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
      return (
        <AppLoading
          startAsync={this._loadResourcesAsync}
          onError={this._handleLoadingError}
          onFinish={this._handleFinishLoading}
        />
      );
    } else {
      return (
        <Provider store={store}>
          <PersistGate loading={null} persistor={persistor}>
            <MainApp />
          </PersistGate>
        </Provider>
        // <SafeAreaView style={{ flex: 1 }}>
        //   <NavigationContainer>
        //     <GalioProvider theme={materialTheme}>
        //       <Block flex>
        //         {Platform.OS === "ios" && <StatusBar barStyle="default" />}
        //         <Screens />
        //         {/* <Login /> */}
        //         {/* <Forgot /> */}
        //         {/* <UnAuNavigation/> */}
        //         {/* <Register /> */}
        //         {/* <Navigation /> */}
        //       </Block>
        //     </GalioProvider>
        //   </NavigationContainer>
        // </SafeAreaView>
      );
    }
  }

  _loadResourcesAsync = async () => {
    return Promise.all([...cacheImages(assetImages)]);
  };

  _handleLoadingError = (error) => {
    // In this case, you might want to report the error to your error
    // reporting service, for example Sentry
    console.warn(error);
  };

  _handleFinishLoading = () => {
    this.setState({ isLoadingComplete: true });
  };
}
